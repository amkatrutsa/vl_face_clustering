clear all
lfw = load('../data/X_lfw.mat');
y_lfw = load('../data/y_lfw.mat');
X = lfw.descriptors;
y = double(y_lfw.y');
th1 = 0.1:0.1:1.1;
th2 = 1.1:0.00001:1.2;
th3 = 1.2:0.1:1.9;
th = [th1, th2, th3];
true_pos = zeros(1, length(th));
false_pos = zeros(1, length(th));
num_clusters = zeros(1, length(th));
Y = pdist(X, 'euclid'); 
Z = linkage(Y, 'single');
parfor i = 1:length(th)
    y_predict = cluster(Z, 'cutoff', th(i));
    num_clusters(i) = length(unique(y_predict));
    [true_pos(i), false_pos(i)] = cluster_quality(y, y_predict);
%     [true_pos(i), false_pos(i)] = vl_roc(y, y_predict);
%     vl_roc(y, y_predict);
end
% [label, score] = convert_to_roc(X, y);
% vl_roc(label, score);
% [tpr, tnr, info] = vl_roc(label, score);
% fpr = 1 - tnr;