folder = '../../frames/online_faces/';
num_camera = '3';
dirData = dir(strcat(folder, num_camera, '/descriptors'));
dirIndex = [dirData.isdir];  %# Find the index for directories
fileList = {dirData(~dirIndex).name}';  %'# Get a list of the files
int_fileList = sort(cellfun(@(x) str2num(x), fileList));
sort_fileList = arrayfun(@(x) num2str(x), int_fileList, 'UniformOutput', false);
if ~isempty(sort_fileList)
    fileList = cellfun(@(x) fullfile(strcat(folder, num_camera, '/descriptors'), x),...  %# Prepend path to files
               sort_fileList,'UniformOutput',false);
end
descriptor = readxpk2(fileList{1});
s = sum(descriptor.cnn_descr' ~= 0);
descriptors = zeros(length(fileList), s);
for i = 1:length(fileList)
    descriptor = readxpk2(fileList{i});
    descriptors(i, :) = descriptor.cnn_descr(1:s)';
end

dir_faces = dir(strcat(folder, num_camera));
dirIndex = [dir_faces.isdir];  %# Find the index for directories
frame_list = {dir_faces(~dirIndex).name}';  %'# Get a list of the files
num_frame = cellfun(@(x) regexp(x, '\.', 'split'), frame_list, 'UniformOutput', false);
num_frame = cellfun(@(x) str2num(x{1}), num_frame, 'UniformOutput', false);
num_frame = sort(cell2mat(num_frame));
sort_frame_list = arrayfun(@(x) strcat(num2str(x), '.ppm'), num_frame, 'UniformOutput', false);
if ~isempty(frame_list)
    frame_list = cellfun(@(x) fullfile(strcat('../../frames/online_faces/', num_camera, '/'), x),...  %# Prepend path to files
               sort_frame_list, 'UniformOutput', false);
end
% [idx, C] = kmeans(descriptors, 2);
save(strcat('../data/X_', num_camera), 'descriptors', 'frame_list');