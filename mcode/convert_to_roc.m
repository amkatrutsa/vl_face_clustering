function [true_label, distances] = convert_to_roc(X, y)
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here
m = length(y) * (length(y) - 1) / 2;
true_label = -2*ones(1, m);
distances = -1*ones(1, m);
k = 1;
for i = 1:length(y)
    for j = (i+1):length(y)
        distances(k) = norm(X(i, :) - X(j, :));
        if (y(i) == y(j))
            true_label(k) = 1;
        else
            true_label(k) = -1;
        end
        k = k + 1;
    end
end
true_label = true_label(true_label ~= -2);
distances = -distances(distances ~= -1);
end