function [distances] = UnsuperDistanceDistrib(X)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here
n = size(X, 1);
distances = zeros(1, n * (n - 1) / 2);
k = 1;
for i = 1:n
    for j = (i+1):n
        distances(k) = norm(X(i, :) - X(j, :));
        k = k + 1;
    end
end
end

