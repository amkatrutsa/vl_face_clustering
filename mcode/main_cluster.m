clear all
% Camera 1 - 
% Camera 2 -
% Camera 3 - 70 clusters
% Camera 4 - 27 clusters
cam_4 = load('../data/X_3.mat');
X = cam_4.descriptors;
frame_list = cam_4.frame_list;
%% Determine maximum of the minimum pairwise distance
D = squareform(pdist(X));
d = zeros(1, size(D, 1));
for i = 1:size(D, 1)
    idx = find(D(i, :) ~= 0);
    d(i) = min(D(i, idx));
end
[v, i] = max(d);
%% Agglomerative clustering, determine the threshold 
frame_list = cam_4.frame_list;
th = 1.15:0.0000005:1.16;
y = zeros(length(th), size(X, 1));
num_clusters = zeros(1, length(th));
Y = pdist(X, 'euclid'); 
Z = linkage(Y, 'single');
parfor i = 1:length(th)
    if (rem(100*th(i), 1) == 0)
        fprintf('%d\n', th(i))
    end
    y(i, :) = cluster(Z, 'cutoff', th(i));
    num_clusters(i) = length(unique(y(i, :)));
end
%% Graph partition clustering, determine threshold
D = squareform(pdist(X));
th = 0.5:0.01:1.3;
num_clusters = zeros(1, length(th));
for i = 1:length(th)
    G = sparse(double(D < th(i)));
    [S, C] = graphconncomp(G);
    num_clusters(i) = length(unique(C));
end