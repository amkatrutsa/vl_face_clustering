function [num_true_pos_pairs, num_false_pos_pairs] = cluster_quality(y_true, y_predict)
%CLUSTER_QUALITY Summary of this function goes here
%   Detailed explanation goes here
num_true_pos_pairs = 0;
num_false_pos_pairs = 0;
num_internal = 0;
num_external = 0;
for i = 1:length(y_predict)
    for j = (i+1):length(y_predict)
        if (y_true(i) == y_true(j))
            num_internal = num_internal + 1;
        else
            num_external = num_external + 1;
        end
        if ((y_true(i) == y_true(j)) && (y_predict(i) == y_predict(j)))
            num_true_pos_pairs = num_true_pos_pairs + 1;
        end
        if ((y_true(i) ~= y_true(j)) && (y_predict(i) == y_predict(j)))
            num_false_pos_pairs = num_false_pos_pairs + 1;
        end
    end
end
num_true_pos_pairs = num_true_pos_pairs / num_internal;
num_false_pos_pairs = num_false_pos_pairs / num_external;
end