function [inner_dist, external_dist] = SuperDistanceDistrib(X, y)
%DIST_DISTRIBUTION Summary of this function goes here
%   Detailed explanation goes here
m = length(y);
inner_dist = [];
external_dist = [];
for i = 1:m
    for j = (i+1):m
        cur_dist = norm(X(i, :) - X(j, :));
        if (y(i) == y(j))
            inner_dist(end + 1) = cur_dist;
        else
            external_dist(end + 1) = cur_dist;
        end
    end
end
end

