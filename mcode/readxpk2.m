function [ bhff ] = readxpk2( fname )
%readxpk Read xpk file and put descriptor in resulting structure
% resulting fields:
% fname
% dscr
% dlen
% metric (?) 
% ffpos
% ffconf

%no check
%let it fail
fd=fopen(fname);

signature = bitor(uint32('X'), bitor(bitshift(uint32('P'), 8), ...
    bitor(bitshift(uint32('K'), 16), bitshift(uint32('F'), 24))));

% check signature
% magic = fread(fd,1,'*uint32','l');
% %also check version
% version = fread(fd,1,'*uint32','l');
% if(magic == signature && version == 110)
%     %get element count
%     elcount = fread(fd,1,'*int32','l');
%     for i = 1:elcount
%         elstruct.type = fread(fd,1,'uint32','l');
%         elstruct.offset = fread(fd,1,'*uint32','l');
%         elstruct.size = fread(fd,1,'int32','l');
%         
%         eltable(i) = elstruct;
%     end
    
    %here we are presuming certain element types ARE present
    %no error checking is made on them
    
%     descrIndex = find([eltable.type] == 0);
%     if (~isempty(descrIndex))
        % read descriptor
%         fseek(fd, eltable(descrIndex).offset, 'bof');
        %read signature
        fread(fd, 1, 'int32', 'l');
%         bhff.dscr = transpose(fread(fd, 256, '*float32', 'l'));
%         bhff.dlen = fread(fd, 1, 'uint16=>int32', 'l');
%         bhff.metric = fread(fd, 1, 'uint16=>int32', 'l');
        bhff.cnn_descr = fread(fd, 5*32, 'single');
%     end	
    
%     fsetIndex = find([eltable.type] == 1);
%     if (~isempty(fsetIndex))
%         % read feature set
%         fseek(fd, eltable(fsetIndex).offset, 'bof');
%         %read signature
%         fread(fd, 1, 'int32', 'l');
%         num_points = 9;
%         bhff.ffpos = fread(fd, num_points*2, 'uint16=>int32', 'l');
%         bhff.ffpos = reshape(bhff.ffpos, 2, num_points);
%         bhff.ffconf = fread(fd, 1, 'uint16', 'l');
%         % normalise confidence
%         bhff.ffconf = bhff.ffconf / 65535.0;
%     end
%     
%     % note: for coordinates in image space, use 3 as type
%     pdetIndex = find([eltable.type] == 3);
%     if (~isempty(pdetIndex))
%         % read detection data in portrait coordinates
%         fseek(fd, eltable(pdetIndex).offset, 'bof');
%         bhff.bbox.x = fread(fd, 1, 'int32', 'l');
%         bhff.bbox.y = fread(fd, 1, 'int32', 'l');
%         bhff.bbox.width = fread(fd, 1, 'int32', 'l');
%         bhff.bbox.height = fread(fd, 1, 'int32', 'l');
%         bhff.score = fread(fd, 1, 'float32', 'l');
%         bhff.yaw = fread(fd, 1, 'float32', 'l');
%         bhff.pitch = fread(fd, 1, 'float32', 'l');
%         bhff.roll = fread(fd, 1, 'float32', 'l');
%     end
% end
fclose(fd);
end

