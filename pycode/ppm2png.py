import os
import cv2
folder = "../../frames/online_faces/3/"
for f in os.listdir(folder):
    if (not f.startswith('.')) and (f != "descriptors"):
        frame = cv2.imread(folder + f)
        os.remove(folder + f)
        cv2.imwrite(folder + f.replace('ppm', 'png'), frame)