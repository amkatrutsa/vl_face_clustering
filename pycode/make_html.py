import scipy.io as sio
import numpy as np
import glob
from dominate import document
from dominate.tags import *
import dominate

mat_file = "../data/cam3_conncomp_result.mat"
frame_list = sio.loadmat(mat_file)["frame_list"]
frame_list = [frame[0][0][:-3] + "png" for frame in frame_list ]
num_components = sio.loadmat(mat_file)["C"][0]
unique_components = np.unique(num_components)
sorted_unique_components = sorted(unique_components, key = lambda x: -np.sum(num_components == x))

with document(title="cam3_conn_components") as doc:
    for i in sorted_unique_components:
        current_idx = np.nonzero(num_components == i)[0]
        print current_idx
        p(h1("Component No. " + str(i)))
        if len(current_idx) > 1:
            if len(current_idx) % 2 == 0:
                for j in current_idx[::2]:
                    table(
                    tr(
                    td(img(src=frame_list[j], height="200", width="300", style="float:left"), _class = "photo"),
                    td(img(src=frame_list[j+1], height="200", width="300", style="float:left"), _class = "photo")
                    )
                    )
            else:
                for j in current_idx[::2][:-1]:
                    table(
                    tr(
                    td(img(src=frame_list[j], height="200", width="300", style="float:left"), _class = "photo"),
                    td(img(src=frame_list[j+1], height="200", width="300", style="float:left"), _class = "photo")
                    )
                    )
                table(
                tr(
                td(img(src=frame_list[j+2], height="200", width="300", style="float:left"), _class = "photo")
                )
                )
        else:
            table(
            tr(
            td(img(src=frame_list[0], height="200", width="300", style="float:left"), _class = "photo")
            )
            )
with open("../data/cam3_conn_components.html", "w") as f:
        f.write(doc.render())
